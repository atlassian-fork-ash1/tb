Changelog for TB CLI

===============================



(Unreleased)
-------------------

- Fix git url parse missing metadata

2.0.4 (2019-02-15)
-------------------



- Make Atlassian organization repo default in first time wizard

- Fix typo in devloop

- Streamline and document release process



2.0.3 (2019-02-13)

-------------------



- Support plugin.MY_PLUGIN.min-version config property to define a min version

- Added a bunch of new prompts and options

- Fix OSX single binary support

- Add new deps



2.0.2 (2019-02-08)

-------------------



- Breaking: completely new version using Python 3, prompt_toolkit, and Cement 3

- Core simplified to non-Atlassian commands and concepts

- Introduced new organization repository that contains an organization's config, team definitions, and plugins

- Better first time script to more easily get repositories configured and cloned



1.1.4 (2019-01-19)

-------------------



- 



1.1.3 (2019-01-18)

-------------------



- 



1.1.2 (2019-01-18)

-------------------



- 



1.1.1 (2019-01-18)

-------------------



- Allow adding comma separated list of configuration locations through `TB_CONFIG` env variable



1.1.0 (2018-12-21)

-------------------



- Breaking: change dev loop command names. 'devloop' to 'olddevloop' and 'newdevloop' to 'devloop'

  New devloop doesn’t make any assumptions about the build tool or commands and therefore can work with any project.

  It allows for the dev loop to be configured under devloop in your tb configuration.

  See `tb/config/devloops.yml` for examples and pre-existing default loops named `gradle-docker` and `maven-docker`.

  So if you are currently using `tb devloop` you can check if it still just works,

  as it should autodetect if you are using maven or gradle and use one of the predefined loops.

  Otherwise you can either define your own dev loop config or switch to `tb olddevloop`.



1.0.27 (2018-11-20)

-------------------



- 



1.0.26 (2018-09-10)

-------------------



- 



1.0.25 (2018-07-20)

-------------------



- 



1.0.24 (2018-07-12)

-------------------



- 



1.0.23 (2018-06-29)

-------------------



- 



1.0.22 (2018-06-27)

-------------------



- 



1.0.21 (2018-06-26)

-------------------



- 



1.0.20 (2018-06-15)

-------------------



- 



1.0.19 (2018-06-15)

-------------------



- 



1.0.18 (2018-06-15)

-------------------



- 



1.0.17 (2018-06-15)

-------------------



- Fixed tb library to work with maven starter

- Fixed tb library to work with non-CPS projects



1.0.16 (2018-04-12)

-------------------



- 



1.0.15 (2018-02-23)

-------------------



- Add --version command



1.0.14 (2018-02-23)

-------------------



- Add update of brew tap to release script



1.0.9 (2018-02-23)

-------------------



- Move config files into tb/config instead of copied into

  user's home directory



1.0.7 (2018-02-23)

-------------------



- Consolidated config in /config directory



1.0.3 (2018-02-22)

-------------------



- Use requirements.txt for dependencies



1.0.2 (2018-02-22)

-------------------



- Release tweaks

- Release instructions on the readme



1.0.1 (2018-02-22)

-------------------



- Initial release

