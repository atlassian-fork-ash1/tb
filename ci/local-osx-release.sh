#!/usr/bin/env bash

# Read Password, we could allow for this via file too...but another time
read -p "Bitbucket Username: " USERNAME
read -p "Bitbucket App Password: " -s APP_PASSWORD
echo

# Run Command
if [ -z "$USERNAME" ] || [ -z "$APP_PASSWORD" ]; then
  echo "Please obtain a Bitbucket app password from bitbucket.org and supply it and your username to release"
  exit 1
fi

BB_AUTH_STRING="$USERNAME:$APP_PASSWORD" ci/release-downloads.sh latest-osx
