REQUIREMENTS_FILE := requirements-dev.txt
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
    REQUIREMENTS_FILE := requirements-osx.txt
endif

.PHONY: clean virtualenv test dist default dist-osx-upload

default: clean virtualenv test dist ;

clean:
	find . -name '*.py[co]' -delete

virtualenv:
	python3 -m venv venv
	venv/bin/pip install pip==18.1
	venv/bin/pip install -r $(REQUIREMENTS_FILE)
	venv/bin/pip install -r ci/requirements.txt
	venv/bin/pip install -e .
	@echo
	@echo "VirtualENV Setup Complete. Now run: source venv/bin/activate"
	@echo

test:
	venv/bin/python -m pytest \
		-v \
		--cov=tb \
		--cov-report=term \
		--cov-report=html:coverage-report \
		tests/ \
		plugins/

dist: clean
	rm -rf dist/*
	venv/bin/pyinstaller tbmain.spec --onefile
	dist/tb -v

dist-osx-upload: clean virtualenv
	source venv/bin/activate
	ci/local-osx-release.sh
	deactivate


