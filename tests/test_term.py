from unittest import mock

from prompt_toolkit.validation import ValidationError
from tb import ColoredTerminal


def test_print():
    with mock.patch('tb.term.print_formatted_text') as html:
        term = ColoredTerminal(theme={'foo': 'yellow'}, history_path=None)
        term.print("<foo>hi Bob</foo>")
        assert html.call_args[1].get('style').style_rules[0] == ('foo', 'yellow')


def test_prompt_menu():
    with mock.patch('tb.term.print_formatted_text') as html:
        term = ColoredTerminal(theme={'foo': 'yellow'}, history_path=None)
        term._prompt = mock.Mock()
        term.prompt_menu("Favorite color", {"blue": "Test best color"})
        assert html.call_args[1].get('style').style_rules[0] == ('foo', 'yellow')


def test_prompt_options():
    term = ColoredTerminal(theme={'foo': 'yellow'}, history_path=None)
    term._prompt = mock.Mock()
    term.prompt("hi Bob", options=['foo', 'bar'])

    doc = mock.Mock()
    doc.text = "foo"
    kwargs = term._prompt.mock_calls[0][2]
    kwargs.get('validator').validate(doc)

    assert 'options' not in kwargs

    try:
        doc.text = 'blah'
        kwargs.get('validator').validate(doc)
    except ValidationError as e:
        assert e.message.startswith("Invalid option:")


def test_prompt_patterns():
    term = ColoredTerminal(theme={'foo': 'yellow'}, history_path=None)
    term._prompt = mock.Mock()
    term.prompt("hi Bob", pattern="foo|Bar")

    doc = mock.Mock()
    doc.text = "foo"
    kwargs = term._prompt.mock_calls[0][2]
    kwargs.get('validator').validate(doc)
    assert 'options' not in kwargs

    try:
        doc.text = 'blah'
        kwargs.get('validator').validate(doc)
    except ValidationError as e:
        assert e.message.startswith("Invalid pattern:")


