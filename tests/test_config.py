
from tb.config import configured_org_repo_dir


def test_configured_org_repo_dir():
    config = dict(
        tb=dict(
            src_dir="/tmp",
            org_repo="git@bitbucket.org:atlassian/foo.git"
        )
    )
    assert "/tmp/foo" == configured_org_repo_dir(config)


def test_configured_org_repo_dir_no_git():
    config = dict(
        tb=dict(
            src_dir="/tmp",
            org_repo="git@bitbucket.org:atlassian/foo"
        )
    )
    assert "/tmp/foo" == configured_org_repo_dir(config)


def test_configured_org_repo_dir_with_https():
    config = dict(
        tb=dict(
            src_dir="/tmp",
            org_repo="https://bitbucket.org/atlassian/foo.git"
        )
    )
    assert "/tmp/foo" == configured_org_repo_dir(config)


def test_configured_org_repo_dir_with_https_no_git():
    config = dict(
        tb=dict(
            src_dir="/tmp",
            org_repo="https://bitbucket.org/atlassian/foo"
        )
    )
    assert "/tmp/foo" == configured_org_repo_dir(config)

