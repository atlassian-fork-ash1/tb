import runpy
from os.path import splitext, basename

from setuptools import setup, find_packages
from setuptools.glob import glob

def get_version():
    file_globals = runpy.run_path("src/tb/version.py")
    return file_globals['__version__']


f = open('README.md', 'r')
LONG_DESCRIPTION = f.read()
f.close()

setup(
    name='tb',
    version=get_version(),
    description='The team b cli',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='Don Brown',
    author_email='don@atlassian.com',
    url='https://bitbucket.org/atlassian/tb',
    license='unlicensed',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    package_data={'tb': ['templates/*']},
    include_package_data=True,
    entry_points="""
        [console_scripts]
        tb = tb.main:main
    """,
)
